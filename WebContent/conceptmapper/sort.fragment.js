sap.ui.jsfragment("conceptmapper.sort", {
	
	createContent: function(oController) {

		
		
		var oSortDialog = new sap.m.Dialog({
			title : "Trier par",
			icon : "sap-icon://sort",
		});
		
		
		oSortDialog.addContent(
				new sap.m.List({
					id : "orderSort",
					headerText : "{i18n>order}",
					mode: "SingleSelect", 
					items : [
						  
					  	new sap.m.StandardListItem({
							title: "{i18n>asc}",
						}),
						
						new sap.m.StandardListItem({
							title: "{i18n>desc}",
							selected: true,
						}),

					
					]
			
					  
				})
		);
		
		oSortDialog.addContent(
				new sap.m.List({
					id : "columnsSort",
					headerText : "{i18n>columns_names}", 
					mode: "SingleSelect", 
					items : [
						  
							new sap.m.StandardListItem({
								title: "{i18n>score}",
								selected: true,
							}),
						  	new sap.m.StandardListItem({
								title: "{i18n>concept_id}",
							}),
							
//							new sap.m.StandardListItem({
//								title: "{i18n>concept_name}",
//							}),
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_code}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>concept_class_id}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>vocabulary_id}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>domain_id}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>standard_concept}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>validity}",
							}),
							
							
							new sap.m.StandardListItem({
								title: "{i18n>frequency}",
							}),
						  
							
							
				  ]
			
					  
			})
		);
		
		oSortDialog.addButton(
                new sap.m.Button({
                    text: "OK",
					icon: "sap-icon://accept",
					type: sap.m.ButtonType.Accept,

					
                    press: function () {
                    	sap.ui.controller("conceptmapper.searchPage").searcher();
                    	oSortDialog.close();
					}
                })
        );
//		oSortDialog.addButton(
//				new sap.m.Button({
//					text: "Reject",
//					icon: "sap-icon://decline",
//					press: function () {
//						oSortDialog.close();
//					}
//				})
//        );
		
		
		return oSortDialog;
		
	}
});