sap.ui.jsview("conceptmapper.userInfosPage", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf conceptmapper.userInfos
	*/ 
	getControllerName : function() {
		return "conceptmapper.userInfosPage";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf conceptmapper.userInfos
	*/ 
	createContent : function(oController) {
		
		
		var oResetPasswordForm = new sap.m.FlexBox({
			alignItems: "Center",
			alignContent: "Center",
			justifyContent: "Center",
			direction: "Column",
			fitContainer: true,
			items: [
				new sap.ui.core.Icon({
					src:"sap-icon://reset",
					size:"5rem"
				}).addStyleClass('icon'),
				
				
				new sap.m.Input({
					id: "modifUsername",
					placeholder: "{i18n>username}",
					value : "{userModel>/m_username}",
					enabled : false,
					type:"Text",
				}).addStyleClass('vBox2'),
				new sap.m.Input({
					id: "modifFirstname",
					placeholder: "{i18n>firstname}",
					value : "{userModel>/m_firstname}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "modifLastname",
					placeholder: "{i18n>lastname}",
					value : "{userModel>/m_lastname}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "modifAddress",
					placeholder: "{i18n>address}",
					value : "{userModel>/m_address}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "modifEmail",
					placeholder: "{i18n>email}",
					value : "{userModel>/m_email}",
					type:"Email",
				}).addStyleClass('vBox2'), 
				
				new sap.m.Text({
					text:"Voulez-vous changer votre mot de passe ?",
					width: "300%"
				}).addStyleClass('resetPwd'),
				new sap.m.RadioButtonGroup({
					columns : 2,
					buttons: [
						new sap.m.RadioButton({
							text: "Non",
							selected: true,
							select : [ "hide", oController.showHidePassword, oController ],
						}),
						new sap.m.RadioButton({
							text: "Oui",
							select : [ "show", oController.showHidePassword, oController ],
						}),
					]
				}),
				new sap.m.Input({
					id: "modifPassword",
					placeholder: "{i18n>password}",
					type:"Password",
					visible: false,
				}).addStyleClass('vBox2'),			
				new sap.m.Input({
					id: "modifConfirmPassword",
					placeholder: "{i18n>password}",
					type:"Password",
					visible: false,
				}).addStyleClass('vBox2'), 
				
				
				
			]
		}).addStyleClass('vBox1');
		
		
		
		
		var oSubHeader = new sap.m.Bar({
			contentMiddle : [ new sap.m.Label({
				text : "{i18n>user_infos}"
			}) ]
		});
		
		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			showNavButton : true,
			navButtonPress : function(oEvt) {
				app.back();
			},

			showSubHeader : true,
			subHeader : oSubHeader,
			content : [ oResetPasswordForm
				 ],
			footer : new sap.m.Bar({
				
				contentMiddle: [new sap.m.Button({
                    text: "{i18n>submit}",
					icon: "sap-icon://accept",
					type: sap.m.ButtonType.Accept,
					press: [ oController.resetPwd, oController ]
				}),],
			}),
				
				
				
		});

 		return oPage;
	}

});