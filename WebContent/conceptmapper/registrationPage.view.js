sap.ui.jsview("conceptmapper.registrationPage", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf conceptmapper.registrationPage
	*/ 
	getControllerName : function() {
		return "conceptmapper.registrationPage";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf conceptmapper.registrationPage
	*/ 
	createContent : function(oController) {
		
		
		var oRegistrationForm = new sap.m.FlexBox({
			alignItems: "Center",
			alignContent: "Center",
			justifyContent: "Center",
			direction: "Column",
			fitContainer: true,
			items: [
				new sap.ui.core.Icon({
					src:"sap-icon://add-employee",
					size:"5rem"
				}).addStyleClass('icon'),
				
				
				
				new sap.m.Input({
					id: "firstname",
					placeholder: "{i18n>firstname}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "lastname",
					placeholder: "{i18n>lastname}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "address",
					placeholder: "{i18n>address}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "email",
					placeholder: "{i18n>email}",
					type:"Email",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "username",
					placeholder: "{i18n>username}",
					type:"Text",
				}).addStyleClass('vBox2'), 
				new sap.m.Input({
					id: "password",
					placeholder: "{i18n>password}",
					type:"Password",
				}).addStyleClass('vBox2'),
				new sap.m.Input({
					id: "confirmPassword",
					placeholder: "{i18n>password}",
					type:"Password",
				}).addStyleClass('vBox2'), 
				new sap.m.Title({
					text:"{i18n>waiter}"
				}).addStyleClass('waiter'),
				
				
			]
		}).addStyleClass('vBox1');
		
		
		
		
		var oSubHeader = new sap.m.Bar({
			contentMiddle : [ new sap.m.Label({
				text : "{i18n>registration_page}"
			}) ]
		});
		
		var oPage = new sap.m.Page({
			title : "{i18n>app_head}",
			showNavButton : true,
			navButtonPress : function(oEvt) {
				app.back();
			},

			showSubHeader : true,
			subHeader : oSubHeader,
			content : [ oRegistrationForm
				 ],
			footer : new sap.m.Bar({
				
				contentMiddle: [new sap.m.Button({
                    text: "{i18n>submit}",
					icon: "sap-icon://accept",
					type: sap.m.ButtonType.Accept,
					press: [ oController.registration, oController ]
				}),],
			}),
				
				
				
		});

 		return oPage;
 		
	}

});