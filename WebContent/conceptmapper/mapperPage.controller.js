function setEasyPreviousNextMapperPage(position, total_concepts) {

	if (total_concepts === 1) {
		sap.ui.getCore().byId('oButtonPrevConceptToMap').setEnabled(false);
		sap.ui.getCore().byId('oButtonNextConceptToMap').setEnabled(false);
	} else if (position == 0) {
		sap.ui.getCore().byId('oButtonPrevConceptToMap').setEnabled(false);
		sap.ui.getCore().byId('oButtonNextConceptToMap').setEnabled(true);
	} else if (position + 1 === total_concepts) {
		sap.ui.getCore().byId('oButtonPrevConceptToMap').setEnabled(true);
		sap.ui.getCore().byId('oButtonNextConceptToMap').setEnabled(false);
	} else {
		sap.ui.getCore().byId('oButtonNextConceptToMap').setEnabled(true);
		sap.ui.getCore().byId('oButtonPrevConceptToMap').setEnabled(true);
	}				



}

function resetMapperProperty() {
	//reset comment and rate
	sap.ui.getCore().byId('oConceptMappingRate').resetProperty('value');
	sap.ui.getCore().byId('oConceptMappingComment').resetProperty('value');
	
	//reset simple algo
	
	sap.ui.getCore().byId("algo1Toggle").setPressed(true);
	sap.ui.getCore().byId("algo2Toggle").setPressed(false);
	//sap.ui.getCore().byId("algo3Toggle").setPressed(false);

}




sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/conceptAjax",
	"./utils/conceptsAjax",
	"./utils/conceptRelationshipAjax",
	"./utils/userAjax",
	"./utils/keyboardEvents",

		], function(Controller, conceptAjax, conceptsAjax, conceptRelationshipAjax, userAjax, keyboardEvents) {
		   "use strict";

return Controller.extend("conceptmapper.mapperPage", {	


	
	newConcept :  function (oEvt, sDirection) { 
			debugger;
			resetMapperProperty();
			var currentPosition = sap.ui.getCore().getModel('conceptsDetailModel').oData.to_map_position;
			if (sDirection === 'prev') {
				currentPosition -= 1;
			} else {
				currentPosition += 1
			}
			var conceptModelDocs =  sap.ui.getCore().getModel('conceptsModel').oData.docs;
			var totalConcepts = conceptModelDocs.length;
			var newConcept = conceptAjax.getNewConceptMapperAjax(conceptModelDocs[currentPosition].concept_id)
			
	        var oDomainMCB_list = [];
	        newConcept.domain_id !== 'unknown' ? oDomainMCB_list.push(newConcept.domain_id):[];
	        var non_translate_string = "";
	        var non_translate_string_language = "";
	        var search_string = "";
	        var to_map_position = -1;
	
	
	        if (newConcept.m_language_id) {
				non_translate_string_language = newConcept.m_language_id;
		        non_translate_string = newConcept.concept_name;
    			search_string = newConcept.translate_api_en;

			} else {
				// add other language
				non_translate_string_language = 'EN';
    			search_string = newConcept.concept_name;
			}
			if (non_translate_string_language === 'EN') {
				sap.ui.getCore().byId("oEnglishButton").setPressed(true);
				sap.ui.getCore().byId("oIntralanguageButton").setPressed(false);
				sap.ui.getCore().byId("oIntralanguageButton").setEnabled(false);
				sap.ui.getCore().byId("oEnglishButton").setEnabled(false);
			} else {
				sap.ui.getCore().byId("oIntralanguageButton").setEnabled(true);
				sap.ui.getCore().byId("oEnglishButton").setEnabled(true);
			}
			
			sap.ui.getCore().getModel('conceptsDetailModel').setProperty('/to_map', [newConcept]);
			sap.ui.getCore().getModel('conceptsDetailModel').oData.to_map_position = currentPosition;


			
			setEasyPreviousNextMapperPage(currentPosition, totalConcepts);
			
			
				
	        
	        var oData = {};
	        
	    	oData.select = ["m_project_type_id", "concept_id", "concept_name", "domain_id", "vocabulary_id", "standard_concept", "concept_class_id", "concept_code", "is_mapped", "score"];
	    
	        oData.search_string = search_string;
	        oData.initial_search_string = search_string; // to compare
//	        oData.standard_concept = ['Standard'];
//	        oData.domain_id = oDomainMCB_list;
	        
	        oData.standard_concept = sap.ui.getCore().getModel('searchFieldMapperModel').oData['standard_concept'];
	        oData.domain_id = sap.ui.getCore().getModel('searchFieldMapperModel').oData['domain_id'];
	        oData.vocabulary_id = sap.ui.getCore().getModel('searchFieldMapperModel').oData['vocabulary_id'];

	        
	        sap.ui.getCore().getModel('searchFieldMapperModel').oData['invalid_reason'];
	        oData.invalid_reason = ['Valid'];
	        
	        
	        if (non_translate_string !== "") {
	        	oData.non_translate_string = non_translate_string;
	        }
	        
	        if (non_translate_string_language !== 'EN' && sap.ui.getCore().byId("oIntralanguageButton").getPressed() === true){
	        	oData.search_string = non_translate_string;
	        }

	        oData.limit = 50;
	        
	        var oModel = new sap.ui.model.json.JSONModel(oData);
			sap.ui.getCore().setModel(oModel, "searchFieldMapperModel");
	        
			conceptsAjax.getConceptsMapperAjax(oData);
		
		
	},
	

	goToConceptDetail :  function (oEvt) {

        // GET AJAX call on concept
		conceptAjax.getConceptDetailAjax(oEvt.getSource().getProperty('text'));

		app.to('idConceptDetailPage', 'slide'); //('fade', 'flip', 'show', 'slide =default')
	},
	
	englishButton :  function (oEvt) {
		
		sap.ui.getCore().byId("oEnglishButton").setPressed(true);
		sap.ui.getCore().byId("oIntralanguageButton").setPressed(false);
		
		var initial_search_string = sap.ui.getCore().getModel('searchFieldMapperModel').oData.initial_search_string;
		sap.ui.getCore().byId("searchFieldMapper").setProperty('value', initial_search_string);
		
		sap.ui.controller("conceptmapper.mapperPage").searcher(oEvt, "simple");



	}, 
	
	intralanguageButton :  function (oEvt) {
		
		sap.ui.getCore().byId("oEnglishButton").setPressed(false);
		sap.ui.getCore().byId("oIntralanguageButton").setPressed(true);
		
		var non_translate_string = sap.ui.getCore().getModel('searchFieldMapperModel').oData.non_translate_string;
		sap.ui.getCore().byId("searchFieldMapper").setProperty('value', non_translate_string);
		
		sap.ui.controller("conceptmapper.mapperPage").searcher(oEvt, "simple");


	}, 
	
	algo_one :  function (oEvt) {
		
		sap.ui.getCore().byId("algo1Toggle").setPressed(true);
		sap.ui.getCore().byId("algo2Toggle").setPressed(false);
//		sap.ui.getCore().byId("algo3Toggle").setPressed(false);
		
		sap.ui.controller("conceptmapper.mapperPage").searcher(oEvt, "simple");
		
	},
	
	algo_two :  function (oEvt) {
		
		if (sap.ui.getCore().getModel('conceptsDetailModel').oData.to_map_number !== 1) {
			
			sap.ui.getCore().byId("algo1Toggle").setPressed(true);
			sap.ui.getCore().byId("algo2Toggle").setPressed(false);
			
			sap.m.MessageToast.show("This algo is not possible with multiple concepts choosen");


		} else if (! sap.ui.getCore().getModel('conceptsDetailModel').oData.to_map[0].m_frequency_id) {
			sap.ui.getCore().byId("algo1Toggle").setPressed(true);
			sap.ui.getCore().byId("algo2Toggle").setPressed(false);
			
			sap.m.MessageToast.show("No frequency provided for this item");

			
			
		}	else  {
			sap.ui.getCore().byId("algo1Toggle").setPressed(false);
			sap.ui.getCore().byId("algo2Toggle").setPressed(true);
			
			
			sap.ui.controller("conceptmapper.mapperPage").searcher(oEvt, "frequency");
			
		}
	},

	
	
	
	searcher : function(oEvt, sAlgo) {
	
		// mapperModel to construct
        var oData = {};

    	oData.select = ["m_project_type_id", "concept_id", "concept_name", "domain_id", "vocabulary_id", "standard_concept", "concept_class_id", "concept_code", "is_mapped", "score"];

		var oStandardMCB_ref = sap.ui.getCore().byId('oStandardMapperMCB').getSelectedItems();
		var oStandardMCB_list = [];
		for (var i in oStandardMCB_ref) {
			oStandardMCB_list.push(oStandardMCB_ref[i].getProperty('key'));
		}
		
		var oDomainMCB_ref = sap.ui.getCore().byId('oDomainMapperMCB').getSelectedItems();
		var oDomainMCB_list = [];
		for (var i in oDomainMCB_ref) {
			oDomainMCB_list.push(oDomainMCB_ref[i].getProperty('key'));
		}
		
		var oVocMCB_ref = sap.ui.getCore().byId('oVocMapperMCB').getSelectedItems();
		var oVocMCB_list = [];
		for (var i in oVocMCB_ref) {
			oVocMCB_list.push(oVocMCB_ref[i].getProperty('key'));
		}

		   
        oData.limit = 50;
        oData.search_string = sap.ui.getCore().byId('searchFieldMapper').getProperty('value');
        oData.non_translate_string_language = sap.ui.getCore().getModel('searchFieldMapperModel').oData.non_translate_string_language;
        if(sap.ui.getCore().getModel('searchFieldMapperModel').oData.non_translate_string) {
            oData.non_translate_string = sap.ui.getCore().getModel('searchFieldMapperModel').oData.non_translate_string;

        }
        oData.initial_search_string = sap.ui.getCore().getModel('searchFieldMapperModel').oData.initial_search_string;
        
        oData.standard_concept = oStandardMCB_list;
        
        
        oData.invalid_reason = ['Valid'];
        oData.domain_id = oDomainMCB_list;
        oData.vocabulary_id = oVocMCB_list;
        
        oData.algo = sAlgo;
        if (sAlgo === 'frequency') {
            oData.frequency = sap.ui.getCore().getModel('conceptsDetailModel').oData.to_map[0].m_frequency_id;
        }

        conceptsAjax.getConceptsMapperAjax(oData);
		
		var oModel = new sap.ui.model.json.JSONModel(oData);
		sap.ui.getCore().setModel(oModel, "searchFieldMapperModel");
		
		
		
	},
	
	addRelation : function(oEvt, sNumberMatching ) {
		
		var URL_concept_relationship = "http://localhost:5000/concept_relationship/";
		
		if (sap.ui.getCore().getModel('loginModel') !== undefined) {
			var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
		}
		
//		conceptsDetailModel
		var concepts = sap.ui.getCore().getModel('conceptsDetailModel').oData.to_map;
		var oData = {};
		
		
		
		oData.m_mapping_rate = sap.ui.getCore().byId("oConceptMappingRate").getProperty('value');
		oData.m_mapping_comment = sap.ui.getCore().byId("oConceptMappingComment").getProperty('value');
		debugger;
		
		if (sNumberMatching != "clickOnRelationTable") {
			oData.concept_id_2 = sNumberMatching;
		} else {
			oData.concept_id_2 = oEvt.getSource().getAggregation('cells')[0].getProperty('text');

		}

		oData.relationship_id = 'Maps to';
		
//		 add helper
		if (sap.ui.getCore().byId("searchFieldMapper").getProperty('value') !== sap.ui.getCore().getModel('searchFieldMapperModel').oData.initial_search_string) {
			oData.helper = sap.ui.getCore().byId("searchFieldMapper").getProperty('value');
		}
		
		if (!sap.ui.getCore().getModel('conceptsDetailModel').oData.expert_mode) {
			

		
				var oMapperDialog = new sap.m.Dialog({
					title : "Mapper",
					icon : "sap-icon://doc-attachment"
				});
				
				var text = sap.ui.getCore().getModel('i18n').getProperty('will_map');
				text += " '" +  (oData.concept_id_2 == "0" ? sap.ui.getCore().getModel('i18n').getProperty('no_matching_concept') : oData.concept_id_2) + "'.\n";
				text += sap.ui.getCore().getModel('i18n').getProperty('sure_question');
				
				oMapperDialog.addContent(new sap.m.Text({text: text}));
		
				oMapperDialog.addButton(
		                new sap.m.Button({
		                    text: "Yes",
							type: "Accept",
							icon: "sap-icon://accept",
							
		                    press: function () {
		                    	
		                    	
		                		for (var id in concepts) {		                			
		                			
		                			debugger;
		                			conceptRelationshipAjax.putConceptRelationshipAjax(concepts[id].concept_id, oData)
		                						
		                		}
		
		                    	
		                    	oMapperDialog.close();
		                    	
		                    	sap.ui.controller("conceptmapper.searchPage").searcher();
		                    	
		                		resetMapperProperty();
		                		
		                		app.to('idSearchPage');

		                    }
		
		                })
		        );
				
				
				oMapperDialog.addButton(
						new sap.m.Button({
							text: "Reject",
							icon: "sap-icon://decline",
							press: function () {
								oMapperDialog.close();
							}
						})
		        );
				
				oMapperDialog.open();
		
		} else {
			
			
			
			for (var id in concepts) {
				conceptRelationshipAjax.putConceptRelationshipAjax(concepts[id].concept_id, oData)
			
    		}

    		
    		
    		if (sap.ui.getCore().byId('oButtonNextConceptToMap').getEnabled() === true) {
    			
            	sap.ui.controller("conceptmapper.mapperPage").newConcept('', 'next');
    			
    		} else {
            	sap.ui.controller("conceptmapper.searchPage").searcher();
            	
        		resetProperty();

        		app.to('idSearchPage');

    		}
        	
			
		}

	},

	userInfos : function (oEvt) {
		usersAjax.getUser(sap.ui.getCore().getModel('loginModel').getProperty('/m_username'))
		app.to('idUserInfosPage');
	}, 
	
	navBack : function (oEvt) {
		resetMapperProperty();
		keyboardEvents.searchPage();
		app.back();

	}, 
	
	logout : function (oEvt) {
		
		sap.ui.controller("conceptmapper.searchPage").logout();
		
		resetMapperProperty();
		sap.ui.getCore().byId('oIcTBMapper').setExpanded(false);

		app.to('idSearchPage');
	
	},
	
	
	createViewSettingsDialog : function(sDialogName){
		var oDialog = this._mViewSettingDialog[sDialogName];
		
		if (!oDialog) {
			debugger;
			oDialog = sap.ui.jsfragment(sDialogName, this); //this = oController object
			this._mViewSettingDialog[sDialogName] = oDialog;
		}
		
		return oDialog;
	},
	
	openCustomizeSPFragment : function(oEvt) {
		this.createViewSettingsDialog("conceptmapper.selectMapperColumns").open();
	},
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf conceptmapper.mapperPage
*/
	onInit: function() {
		
		this._mViewSettingDialog = {};
		
		var oModel = new sap.ui.model.json.JSONModel();
		var mData = {
			"selected" : ['Maps to'],
			"items" : [{
				"key" : "Maps to",
				"text" : "Maps to"
			},
			{
				"key" : "Mapped from",
				"text" : "Mapped from"
			}]
		};
		oModel.setData(mData);
		sap.ui.getCore().setModel(oModel, "relationsModel");

	

	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf conceptmapper.mapperPage
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf conceptmapper.mapperPage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf conceptmapper.mapperPage
*/
//	onExit: function() {
//
//	}

});
});