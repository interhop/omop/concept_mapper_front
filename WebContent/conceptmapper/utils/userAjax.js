sap.ui.define([], function() {
   "use strict";

   return {
	   
	   getUser : function (sUsername) {
	    	sap.ui.core.BusyIndicator.show(0);

		 	var URL_user = "http://localhost:5000/user/"
			
			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			if (sUsername !== undefined) {

			
				$.ajax({
					method:"GET",
					url: URL_user + sUsername,
					dataType:"json",
					async: false,
					header: {
						"Content-Type": "application/json",
					},
					beforeSend: function(xhr, settings) {xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
					success: function(data, response, xhr) {
						if (data.m_invalid_reason === 'D') {
							data.m_invalid_reason = 'Invalid'
						} else {
							data.m_invalid_reason = 'Valid'
			
						}
						
						var oModel = new sap.ui.model.json.JSONModel(data);
						sap.ui.getCore().setModel(oModel, "userModel");
						console.log("Connection : getUser()");
					},
					error : function(error) {
						sap.m.MessageToast.show("Connexion error");
						console.log("Connection error : getUser()");
						console.log(error);
					}
				});
				
			}
	    	sap.ui.core.BusyIndicator.hide();

		}, 

		postUserAjax : function (oData, sUsername) {
			
	    	sap.ui.core.BusyIndicator.show(0);

		 	var URL_user = "http://localhost:5000/user/";
			
			
			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			if (sUsername !== undefined) {
				$.ajax({
					type:"POST",
					url: URL_user + sUsername,
					dataType:"json",
					async: false,
					data: oData,
					header: {
						"Content-Type": "application/json",
					},
					beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
			
					success: function(data, response, xhr) {

			        	sap.m.MessageToast.show("'" + sUsername + "' has been created! Wait a little for activation");
						console.log("Connection : postUserAjax()");
					},
					error : function(error) {
						sap.m.MessageToast.show("Connexion error");
						console.log("Connection error : postUserAjax()");
						console.log(error);
					}
				});
			}
	    	sap.ui.core.BusyIndicator.hide();

		}, 

		updateUserAjax : function (oData, sUsername) {
	    	sap.ui.core.BusyIndicator.show(0);

			
		 	var URL_user = "http://localhost:5000/user/";
			
			
			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			if (sUsername !== undefined) {

				$.ajax({
					type:"PUT",
					url:URL_user + sUsername,
					dataType:"json",
					data: oData,
					async: false,
					header: {
						"Content-Type": "application/json",
					},
					beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
					success: function(data, response, xhr) {

				    	sap.m.MessageToast.show("New user : '" + sUsername + "' updated");
						console.log("Connection : updateUserAjax()");

					},
					error : function(error) {
						sap.m.MessageToast.show("Connexion error");
						console.log("Connection error : updateUserAjax()");
						console.log(error);
					}
				});
			}

	    	sap.ui.core.BusyIndicator.hide();

			
			
		},
		
		resetAjax : function (oData, sUsername) {
			
	    	sap.ui.core.BusyIndicator.show(0);

		 	var URL_user = "http://localhost:5000/user/";
			
			
			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			if (sUsername !== undefined) {
				$.ajax({
					type:"POST",
					url: URL_user + sUsername,
					dataType:"json",
					async: false,
					data: oData,
					header: {
						"Content-Type": "application/json",
					},
					beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
			
					success: function(data, response, xhr) {

			        	sap.m.MessageToast.show(sUsername + " : your password has been changed");
						console.log("Connection : resetPasswordAjax()");
					},
					error : function(error) {
						sap.m.MessageToast.show("Connexion error");
						console.log("Connection error : resetPasswordAjax()");
						console.log(error);
					}
				});
			}
	    	sap.ui.core.BusyIndicator.hide();

		},
		
		
		changeProjectsAjax : function (oData, sUsername) {
			
	    	sap.ui.core.BusyIndicator.show(0);

		 	var URL_project = "http://localhost:5000/project/";
		 	debugger;
			

			if (sap.ui.getCore().getModel('loginModel') !== undefined) {
				var token = sap.ui.getCore().getModel('loginModel').oData.access_token;
			}
			
			if (sUsername !== undefined) {
				$.ajax({
					type:"PUT",
					url: URL_project + sUsername,
//					dataType:"json",
//					async: false,
			   		data: oData,

//					header: {
//						"Content-Type": "application/json",
//					},
					beforeSend: function(xhr, settings) { xhr.setRequestHeader('Authorization','Bearer ' + token); } ,
			
					success: function(data, response, xhr) {

			        	sap.m.MessageToast.show(sUsername + " : role has been changed");
						console.log("Connection : changeProjectsAjax()");
					},
					error : function(error) {
						sap.m.MessageToast.show("Connexion error");
						console.log("Connection error : changeProjectsAjax()");
						console.log(error);
					}
				});
			}
	    	sap.ui.core.BusyIndicator.hide();

		},
		


   
   };
});