function setBasicResearchParams() {
	//standard, valid, domain, voc, lang, mini, max, search
	
	var oStandardMCB_ref = sap.ui.getCore().byId('oStandardMCB').getSelectedItems();
	var oStandardMCB_list = [];
	for (var i in oStandardMCB_ref) {
		oStandardMCB_list.push(oStandardMCB_ref[i].getProperty('key'));
	}
	
	var oValidMCB_ref = sap.ui.getCore().byId('oValidMCB').getSelectedItems();
	var oValidMCB_list = [];
	for (var i in oValidMCB_ref) {
		oValidMCB_list.push(oValidMCB_ref[i].getProperty('key'));
	}
	
	var oDomainMCB_ref = sap.ui.getCore().byId('oDomainMCB').getSelectedItems();
	var oDomainMCB_list = [];
	for (var i in oDomainMCB_ref) {
		oDomainMCB_list.push(oDomainMCB_ref[i].getProperty('key'));
	}
	
	var oVocMCB_ref = sap.ui.getCore().byId('oVocMCB').getSelectedItems();
	var oVocMCB_list = [];
	for (var i in oVocMCB_ref) {
		oVocMCB_list.push(oVocMCB_ref[i].getProperty('key'));
	}
	
	var oLangMCB_ref = sap.ui.getCore().byId('oLangMCB').getSelectedItems();
	var oLangMCB_list = [];
	for (var i in oLangMCB_ref) {
		oLangMCB_list.push(oLangMCB_ref[i].getProperty('key'));
	}
	
	var oMapMCB_ref = sap.ui.getCore().byId('oMapMCB').getSelectedItems();
	var oMapMCB_list = [];
	for (var i in oMapMCB_ref) {
		oMapMCB_list.push(oMapMCB_ref[i].getProperty('key'));
	}
	
	var oProjectTypeMCB_ref = sap.ui.getCore().byId('oTypeProject').getSelectedItems();
	var oProjectTypeMCB_list = [];
	for (var i in oProjectTypeMCB_ref) {
		oProjectTypeMCB_list.push(oProjectTypeMCB_ref[i].getProperty('key'));
	}
	

//	var oProjectTypeCB_ref = sap.ui.getCore().byId('oTypeProject').getSelectedItem();
//	if (oProjectTypeCB_ref) {
//		var oProjectTypeMCB_list = [];
//		oProjectTypeMCB_list.push(oProjectTypeCB_ref.getProperty('key'));
//	}
	
	var data = {};
	data.m_project_type_id = oProjectTypeMCB_list;
	data.m_language_id = oLangMCB_list;
	data.is_mapped = oMapMCB_list;
	data.domain_id = oDomainMCB_list;
	data.vocabulary_id = oVocMCB_list;
	data.invalid_reason = oValidMCB_list ;
	data.standard_concept = oStandardMCB_list;

	var oModel = new sap.ui.model.json.JSONModel(data);
	sap.ui.getCore().setModel(oModel, "selectedParams");
	

	

	var oData = {};
	
	oData.select = ["concept_id", "concept_name", "domain_id", "vocabulary_id", "frequency", "m_project_type_id",
		"invalid_reason", "standard_concept", "concept_class_id", "concept_code", "is_mapped"];
	
	oData.mini = parseInt(sap.ui.getCore().byId('oMiniIB').getProperty('value'));
    oData.maxi = parseInt(sap.ui.getCore().byId('oMaxiIB').getProperty('value'));
    
    oData.search_string = sap.ui.getCore().byId('searchField').getProperty('value');

    oData.standard_concept = oStandardMCB_list;
    
    
    oData.invalid_reason = oValidMCB_list;
    oData.domain_id = oDomainMCB_list;
    oData.vocabulary_id = oVocMCB_list;
    oData.m_language_id = oLangMCB_list;
    oData.is_mapped = oMapMCB_list;
    oData.m_project_type_id = oProjectTypeMCB_list;
    
    var oRowsNumber = parseInt(sap.ui.getCore().byId('oRowInPageInput').getProperty('value'));
    
    if (!isNaN(oRowsNumber) && Number.isInteger(oRowsNumber)) {
        oData.limit = oRowsNumber;
    }
    
    if (sap.ui.getCore().byId("orderSort")) {
    	if (sap.ui.getCore().byId("orderSort").getAggregation('items')[1].getProperty('selected')) {
    		oData.sort_order = 'desc';
    	} else {
    		oData.sort_order = 'asc';
    	}
    }
   
    if (sap.ui.getCore().byId("columnsSort")) {
		for (var i in sap.ui.getCore().byId("columnsSort").getAggregation('items')) {
			if (sap.ui.getCore().byId("columnsSort").getAggregation('items')[i].getProperty('selected')) {
				oData.sort_column = i;
				break;
			}
		}
    }
    
    return oData;

}


var oItemTemplateValid = new sap.ui.core.Item({
	key : "{isValidModel>key}",
	text : "{isValidModel>text}",
	enabled : "{isValidModel>enabled}"
});

var oItemTemplateStandard = new sap.ui.core.Item({
	key : "{isStandardModel>key}",
	text : "{isStandardModel>text}",
	enabled : "{isStandardModel>enabled}"
});











sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./utils/userAjax",
	"./utils/usersAjax",
	"./utils/conceptAjax",
	"./utils/conceptsAjax",
	"./utils/keyboardEvents",
	"./utils/projectsAjax",
	"./utils/loginAjax",

	"./utils/loginKeyboardEvents",



		], 

		
		
		
		function(Controller, userAjax, usersAjax, conceptAjax, conceptsAjax, keyboardEvents, projectsAjax, loginAjax, loginKeyboardEvents) {
		   "use strict";

return Controller.extend("conceptmapper.searchPage", {	
	
	
	
	searcher : function (oEvt) {
		
		var oData = setBasicResearchParams();

        
		parseInt(sap.ui.getCore().byId('oSearchFielGoToPage').resetProperty('value'));
		
		
		
        // GET AJAX call on conceptSSSSS
//
//		if (!sap.ui.getCore().getModel('conceptsModel') || sap.ui.getCore().byId('searchField').getProperty('value') !== 
//			sap.ui.getCore().getModel('conceptsModel').getProperty('/search_string')) {
//			data = {};
//			data.search_string = oData.search_string;
//			data.select = oData.select;
//			getFaceting(data);
//		}

		conceptsAjax.getConceptsAjax(oData);

		var oModel = sap.ui.getCore().getModel('conceptsModel');
        var aPageFound  = oModel.getProperty("/page_found");
        var aPageShown  = oModel.getProperty("/page_shown");
        if (aPageFound === 0 || aPageFound === 1) {
        	sap.ui.getCore().byId('oButtonPrev').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNext').setEnabled(false);
        } else {
        	sap.ui.getCore().byId('oButtonPrev').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNext').setEnabled(true);
        }		
        
        var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + ++aPageShown;
        var buttonTextFound = sap.ui.getCore().getModel('i18n').getProperty('total_pages') + ' : ' + aPageFound;
        
        sap.ui.getCore().byId('oButtonCurrentPage').setText(buttonTextShown);
        sap.ui.getCore().byId('oButtonTotalPages').setText(buttonTextFound);
        

	},
	
	
	
	prevPage :  function (oEvt) {
		
		var oData = setBasicResearchParams();

		sap.ui.getCore().byId('oSearchFielGoToPage').resetProperty('value');

		var oModel = sap.ui.getCore().getModel('conceptsModel');
        var aPageShown  = oModel.getProperty("/page_shown");
        
        oData.page = aPageShown;
        
        if (oData.page <= 1) {
        	sap.ui.getCore().byId('oButtonPrev').setEnabled(false);
        }
    	sap.ui.getCore().byId('oButtonNext').setEnabled(true);

    	conceptsAjax.getConceptsAjax(oData);
		
		var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + oData.page;
        
        sap.ui.getCore().byId('oButtonCurrentPage').setText(buttonTextShown);
        	
	},
	
	
	nextPage :  function (oEvt) {
		
		var oData = setBasicResearchParams();
		sap.ui.getCore().byId('oSearchFielGoToPage').resetProperty('value');

		
		var oModel = sap.ui.getCore().getModel('conceptsModel');
        var aPageShown  = oModel.getProperty("/page_shown");
        var aPageFound  = oModel.getProperty("/page_found");
        
        if (aPageShown + 1 === aPageFound) {
        	sap.ui.getCore().byId('oButtonNext').setEnabled(false);
        }
    	sap.ui.getCore().byId('oButtonPrev').setEnabled(true);

        oData.page = aPageShown + 2;

        conceptsAjax.getConceptsAjax(oData);
		
        var buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + oData.page;
        
        sap.ui.getCore().byId('oButtonCurrentPage').setText(buttonTextShown);

		
		
	},
	
	
	searcherGoToPage :  function (oEvt) {

		
		var oData = setBasicResearchParams();
		
        oData.page = parseInt(sap.ui.getCore().byId('oSearchFielGoToPage').getProperty('value'));
        
        if (oData.page <= 0 || !oData.page) {
        	oData.page = 1;
        } 
        
		        
        
        
        // GET AJAX call on conceptSSSSS
        conceptsAjax.getConceptsAjax(oData);
		
		var oModel = sap.ui.getCore().getModel('conceptsModel');
        var aPageFound  = oModel.getProperty("/page_found");
        var aPageShown  = oModel.getProperty("/page_shown");
        
        if (oData.page > aPageFound) {
        	oData.page = aPageFound;
        	sap.ui.getCore().byId('oSearchFielGoToPage').setProperty('value', aPageFound);
        	conceptsAjax.getConceptsAjax(oData);

        }
        
        if (aPageFound === 0 || aPageFound === 1) {
        	sap.ui.getCore().byId('oButtonPrev').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNext').setEnabled(false);
        } else if (aPageShown === 0 && aPageShown + 1 !== aPageFound) {
           	sap.ui.getCore().byId('oButtonPrev').setEnabled(false);
        	sap.ui.getCore().byId('oButtonNext').setEnabled(true);
        } else if (aPageShown + 1 === aPageFound) {
        	sap.ui.getCore().byId('oButtonPrev').setEnabled(true);
        	sap.ui.getCore().byId('oButtonNext').setEnabled(false);
        } else {
          	sap.ui.getCore().byId('oButtonPrev').setEnabled(true);
        	sap.ui.getCore().byId('oButtonNext').setEnabled(true);
        }

        
        buttonTextShown = sap.ui.getCore().getModel('i18n').getProperty('current_page') + ' : ' + ++aPageShown;
        buttonTextFound = sap.ui.getCore().getModel('i18n').getProperty('total_pages') + ' : '  + aPageFound;
        
        sap.ui.getCore().byId('oButtonCurrentPage').setText(buttonTextShown);
        sap.ui.getCore().byId('oButtonTotalPages').setText(buttonTextFound);
		

		
		
	},
	
	
	
	
	
	goToConceptDetail :  function (oEvt) {
		
    	// active keyboard
		keyboardEvents.conceptDetailPage();
		
		var sVal = oEvt.getSource().getAggregation('cells')[1].getProperty('text');
		
        // GET AJAX call on concept
		conceptAjax.getConceptDetailAjax(sVal);
				
		app.to('idConceptDetailPage', 'slide'); //('fade', 'flip', 'show', 'slide =default')
	},
	
	
	goToMapperPage :  function (oEvt) {
		
		
		var URL_concept = "http://localhost:5000/concept/";
		
		
		var idx = []
		var oTable_ref = sap.ui.getCore().byId('oTable');
		for (var i in oTable_ref.getAggregation('items')) {
			if (oTable_ref.getAggregation('items')[i].getAggregation('cells')[0].getProperty('selected')) {
				idx.push(i);
			}
		}

        if (idx.length === 0) {
        	sap.m.MessageToast.show("No item to map!");
        	return;
        }
        var listToMap = [];
        
        var oDomainMCB_list = [];
        var non_translate_string = "";
        var non_translate_string_language = "";
        var search_string = "";
        var to_map_position = -1;

        
    	if (sap.ui.getCore().getModel('loginModel') !== undefined) {
    		var token = sap.ui.getCore().getModel('loginModel').oData.access_token;

    	}
        
        for (var i in idx) {
  
        	var domain_id = oTable_ref.getAggregation('items')[idx[i]].getAggregation('cells')[6].getProperty('text');
        	oDomainMCB_list.indexOf(domain_id) === -1 && domain_id !== 'unknown' ? oDomainMCB_list.push(domain_id):console.log("This item already exists");
        	
        	$.ajax({
        		type:"GET",
        		url: URL_concept + oTable_ref.getAggregation('items')[idx[i]].getAggregation('cells')[1].getProperty('text'),
        		dataType:"json",
        		async: false,
        		header: {
        			"Content-Type": "application/json",
        		},
        		success: function(data, response, xhr) {
        			data.status = 'None';
    				data.is_mapped = 'Not-Mapped';
        			if (data.relations) {
        				for (var rel in data.relations) {
        					if (data.relations[rel].invalid_reason === "Valid") {
        						data.status = 'Success';
                				data.is_mapped = "Mapped";
                				break;
        					}
        					
        				}
        			}
        			listToMap.push(data);
        			debugger;
        			if (i == 0) {
        				to_map_position = parseInt(idx[0]);
            			if (data.m_language_id) {
            				non_translate_string_language = data.m_language_id;
            		        non_translate_string = data.concept_name;
                			search_string = data.translate_api_en;

            			} else {
            				// add other language
            				non_translate_string_language = 'EN';
                			search_string = data.concept_name;
            			}
            			if (non_translate_string_language === 'EN') {
            				sap.ui.getCore().byId("oEnglishButton").setPressed(true);
            				sap.ui.getCore().byId("oIntralanguageButton").setPressed(false);
            				sap.ui.getCore().byId("oIntralanguageButton").setEnabled(false);
            				sap.ui.getCore().byId("oEnglishButton").setEnabled(false);

            			} else {
            				sap.ui.getCore().byId("oIntralanguageButton").setEnabled(true);
            				sap.ui.getCore().byId("oEnglishButton").setEnabled(true);
            			}
            			

        			}

        			console.log("connection");
        		},
        		beforeSend: function(xhr, settings) {  xhr.setRequestHeader('Authorization','Bearer ' + token); } ,

        		error : function(error) {
        			alert("pb in the connection");
        			console.log(error);
        		}
        	})

        }
        var oData = {
        		"to_map": listToMap, 
        		"to_map_number": idx.length,
        		"to_map_position": to_map_position,
        		expert_mode: sap.ui.getCore().byId("expertMode").getPressed()
        		};
        
        // easy next previous
    	if (idx.length === 1) {
    		sap.ui.getCore().byId('oButtonPrevConceptToMap').setVisible(true);
    		sap.ui.getCore().byId('oButtonNextConceptToMap').setVisible(true);
    	} else {
    		sap.ui.getCore().byId('oButtonPrevConceptToMap').setVisible(false);
    		sap.ui.getCore().byId('oButtonNextConceptToMap').setVisible(false);
    	}

        setEasyPreviousNextMapperPage(to_map_position, sap.ui.getCore().getModel('conceptsModel').oData.docs.length);

        
        
        var oModel = new sap.ui.model.json.JSONModel(oData);
		sap.ui.getCore().setModel(oModel, "conceptsDetailModel");
		
		
			
        
        var oData = {};
        
    	oData.select = ["m_project_type_id", "concept_id", "concept_name", "domain_id", "vocabulary_id", "standard_concept", "concept_class_id", "concept_code", "is_mapped", "score"];

        oData.search_string = search_string;
        oData.initial_search_string = search_string; // to compare
        oData.standard_concept = ['Standard'];
        oData.domain_id = oDomainMCB_list;
        oData.invalid_reason = ['Valid'];
        oData.vocabulary_id = [];
        oData.non_translate_string_language = non_translate_string_language;

        debugger;
        
        if (non_translate_string !== "") {
        	oData.non_translate_string = non_translate_string;
        }
        if (non_translate_string_language !== 'EN' && sap.ui.getCore().byId("oIntralanguageButton").getPressed() === true){
        	oData.search_string = non_translate_string;
        }

        oData.limit = 50;
        
        var oModel = new sap.ui.model.json.JSONModel(oData);
		sap.ui.getCore().setModel(oModel, "searchFieldMapperModel");
                
		conceptsAjax.getConceptsMapperAjax(oData);
		

		app.to('idMapperPage', 'slide');
		keyboardEvents.mapperPage();

	},
	
	
	
	goToUserPage : function (oEvt) {
		projectsAjax.setAvailable();
		usersAjax.getUsers();

		app.to('idUserPage', 'slide'); 
	},
	
	
	
	
	userAuthentification : function (oEvt) {
		
		var oAuthDialog = new sap.m.Dialog({
			title : "{i18n>connexion}",
			icon : "sap-icon://employee"
		});
				

		

		var oSimpleForm = new sap.ui.layout.form.SimpleForm({
			toolbar: new sap.m.Toolbar({
				content : [
					new sap.m.ToolbarSpacer(),

					new sap.m.Button({
						text:"{i18n>first_time}",
						type: sap.m.ButtonType.Reject,
						press: function(){
	                        oAuthDialog.close();

							app.to('idRegistrationPage');


						},
					}), 
					
					new sap.m.ToolbarSpacer(),
					
				]
			}).addStyleClass('border-bottom'),
			
			content : new sap.m.FlexBox({
				direction:"Column",
				items : [

					new sap.m.Input({
						placeholder: "{i18n>username}",
					}), 
					
					 new sap.m.Input({
							placeholder: "{i18n>password}",
							type:"Password",
						}),

					
				]
			})

		});
		



		oAuthDialog.addContent(oSimpleForm);
		
		
		
		

		oAuthDialog.addButton(
                new sap.m.Button({
                    text: "Log In",
					icon: "sap-icon://accept",
					type: sap.m.ButtonType.Accept,
					
                    press: function () {
		           		 var content = oSimpleForm.getContent()[0];
		                 
		                 var oData = {};
		                 oData.m_username = content.getAggregation('items')[0].getProperty('value');
		                 oData.m_password = content.getAggregation('items')[1].getProperty('value');     	
                       
                        
                        if (oData.m_username !== "" && oData.m_password !== "" ) {
  
                        	loginAjax.postLoginAjax(oData);

                    	 
                        } else {
                        	sap.m.MessageToast.show("You have to fill all the fields");
                        }
                        oAuthDialog.close();
					}
                })
        );
		oAuthDialog.addButton(
				new sap.m.Button({
					text: "Close",
					icon: "sap-icon://decline",
					press: function () {
						oAuthDialog.close();
					}
				})
        );
		
		
		
		loginKeyboardEvents.oAuthDialogOpen(oAuthDialog, oSimpleForm);	
		
		oAuthDialog.open();




	},
	
	userInfos : function (oEvt) {
		userAjax.getUser(sap.ui.getCore().getModel('loginModel').getProperty('/m_username'))
		app.to('idUserInfosPage');
	}, 

	goToReviewerMapper : function (oEvt) {
		sap.ui.controller("conceptmapper.reviewerPage").searchRelation();
		keyboardEvents.reviewerPage();

		app.to('idReviewerPage', 'flip');
	}, 
	
	
	logout : function (oEvt) {
//		RESET MapperPage
		resetMapperProperty();

//		RESET ReviewerPage
		resetReviewerProperty();
//		RESET header
		sap.ui.getCore().byId("oSearchFielGoToPage").setEnabled(false);
		sap.ui.getCore().byId("oRowInPageInput").setEnabled(false);
		sap.ui.getCore().byId("searchField").setEnabled(false);
		sap.ui.getCore().byId("mapper").setEnabled(false);
		sap.ui.getCore().byId("hideShowFilter").setEnabled(false);
		sap.ui.getCore().byId("sortSearchPage").setEnabled(false);
		sap.ui.getCore().byId("customizeSearchPage").setEnabled(false);
		sap.ui.getCore().byId("registrationSearch").setVisible(true);
		sap.ui.getCore().byId("loginSearch").setVisible(false);
		sap.ui.getCore().byId("logoutSearch").setVisible(false);
		sap.ui.getCore().byId("numFound").setVisible(false);
		sap.ui.getCore().byId("usersParam").setVisible(false);
		sap.ui.getCore().byId("switchToReviewer").setEnabled(false);

		
		
		
		
//		RESET CONTROLS
		
		parseInt(sap.ui.getCore().byId('oSearchFielGoToPage').resetProperty('value'));
		sap.ui.getCore().byId('oMiniIB').resetProperty('value');
		sap.ui.getCore().byId('oMaxiIB').resetProperty('value');
		sap.ui.getCore().byId('searchField').resetProperty('value');
		
		sap.ui.getCore().byId('oStandardMCB').setSelectedKeys(['']);
		sap.ui.getCore().byId('oValidMCB').setSelectedKeys(['']);
		sap.ui.getCore().byId('oDomainMCB').setSelectedKeys(['']);
		sap.ui.getCore().byId('oVocMCB').setSelectedKeys(['']);
		sap.ui.getCore().byId('oLangMCB').setSelectedKeys(['FR']);
		sap.ui.getCore().byId('oMapMCB').setSelectedKeys(['']);


		
		sap.ui.getCore().byId('oButtonCurrentPage').setText('');
	    sap.ui.getCore().byId('oButtonTotalPages').setText('');
		
	    
	    sap.ui.getCore().byId('oButtonPrev').setEnabled(false);
    	sap.ui.getCore().byId('oButtonNext').setEnabled(false);	
		
		var oData = setBasicResearchParams();
		oData.m_language_id = [];
		

		
        // GET AJAX call on conceptSSSSS
		
 	    if (! sap.ui.getCore().byId("__container1-Master").hasStyleClass("toggle-masterPage")) {
 	    	sap.ui.getCore().byId("__container1-Master").toggleStyleClass("toggle-masterPage");
 	    }
 	    
 	   var oModel = sap.ui.getCore().getModel("conceptsModel");          //Get Hold of the Model
 	   oModel.setProperty('/docs', []);
 	   oModel.destroy();
 	    
 	   var oModel = sap.ui.getCore().getModel("loginModel");          //Get Hold of the Model
 	   oModel.destroy();
		keyboardEvents.initiationApp();


	},
	
	createViewSettingsDialog : function(sDialogName){
		var oDialog = this._mViewSettingDialog[sDialogName];
		
		if (!oDialog) {
			oDialog = sap.ui.jsfragment(sDialogName, this); //this = oController object
			this._mViewSettingDialog[sDialogName] = oDialog;
		}
		
		return oDialog;
	},
	
	openSortSPFragment : function(oEvt) {
		this.createViewSettingsDialog("conceptmapper.sort").open();
	},
	
	openCustomizeSPFragment : function(oEvt) {
		this.createViewSettingsDialog("conceptmapper.selectColumns").open();
	},
	
	handleSortDialogConfirm : function(oEvt) {
		debugger;
		var params = oEvt.getParameters();
		params.sortDescending
		params.sortItem.getProperty()
	},
	
	
	
	
//	openSortSPFragment : function(oEvt) {
//		if (this.oFragment === undefined) { // because will create a new oFramgent object each type we click
//			this.oFragment = sap.ui.jsfragment("conceptmapper.sort", this); //this = oController object
//			debugger;
//			this.oFragment.open();
//			
//		} else {
//			this.oFragment.open();
//		}
//	},


	
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf conceptmapper.searchPage
*/


	onInit: function(oEvt) {
		
			this._mViewSettingDialog = {};
		
		
			sap.ui.getCore().byId("__container1-Master").addStyleClass('searchPage');
			
			// Hide Navbutton
			sap.ui.getCore().byId("__page1-navButton").addStyleClass('__page1-navButton');
			
			// Hide Master at the beginning
			sap.ui.getCore().byId("__container1-Master").toggleStyleClass("toggle-masterPage");
			

			
			var oModel = new sap.ui.model.json.JSONModel();
			var mData = {
				"selected" : "15",
				"items" : [{
					"key" : "15",
					"text" : "15"
				},
				{
					"key" : "30",
					"text" : "30"
				},
				{
					"key" : "50",
					"text" : "50"
				},
				{
					"key" : "100",
					"text" : "100"
				},
				{
					"key" : "200",
					"text" : "200"
				},
				]
			};
			oModel.setData(mData);
			sap.ui.getCore().setModel(oModel, "RowsPerLignModel");
			
			
			
			
			// LANGUAGE initiation - to test add ?sap-ui-language=da

			var sLoc = sap.ui.getCore().getConfiguration().getLanguage();
			var i18nPath = "i18n/i18n";
			if (sLoc === "fr") {
				i18nPath = i18nPath + "_fr.properties";
			} else if (sLoc === "da") {

				i18nPath = i18nPath + "_da.properties";
			} else if (sLoc === "de") {

				i18nPath = i18nPath + "_de.properties";
			} else if (sLoc === "it") {

				i18nPath = i18nPath + "_it.properties";
			} else if (sLoc === "es") {

				i18nPath = i18nPath + "_es.properties";
			} else {

				i18nPath = i18nPath + ".properties";
			}

			var oi18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl : i18nPath
			});
			sap.ui.getCore().setModel(oi18nModel, "i18n");

			
			
			keyboardEvents.initiationApp();



			
	},
	

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf conceptmapper.searchPage
*/
	onBeforeRendering: function() {		
	
	// to suppress when many pages
	//sap.ui.getCore().byId("__page2-intHeader-BarLeft").addStyleClass('__page2-intHeader-BarLeft');

	
	

	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf conceptmapper.searchPage
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf conceptmapper.searchPage
*/
//	onExit: function() {
//
//	}

});
});